package logging;

import java.io.File;
import java.io.IOException;
import java.io.FileWriter;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

public class LogToFile {
  private String location;

    public LogToFile(String location){
      //This is the user directory, without the \out\
      this.location = location;
    };

    public void writeToFile(String message){
      //Gets current time/date and puts it in a year/month/date format.
      DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
      LocalDateTime now = LocalDateTime.now();
      String date = dtf.format(now);

      //Creates new .txt-file in the project folder's out-folder.
      File logFile = new File(location + "/out/logging.txt");
      //If the file exists, try to write to it, if not - create file and then write to the file.
      if (logFile.exists()){
        try {
          FileWriter writing = new FileWriter(logFile, true);
          writing.write(date + ": " + message + "\n");
          writing.close();
        } catch (IOException e){
          e.printStackTrace();
        };
      } else {
        try {
          logFile.createNewFile();
          FileWriter writing = new FileWriter(logFile);
          writing.write(date + ": " + message + "\n");
          writing.close();
        } catch (IOException e){
          e.printStackTrace();
      };
    };
  };

};
