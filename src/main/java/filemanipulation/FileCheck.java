package filemanipulation;

import java.io.File;
import java.nio.file.*;
import java.util.stream.*;
import java.io.IOException;
import java.io.FilenameFilter;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class FileCheck{

  public FileCheck(){
  }

  public File getSingleFile(String current){
    File chosenFile = null;
    List<File> fileResults = null;

    try (Stream<Path> walk = Files.walk(Paths.get(current))){
      //Goes through all the paths in all the directories, maps them to files and puts them in a list.
      fileResults = walk.filter(Files::isRegularFile)
      .map(x -> x.toFile()).collect(Collectors.toList());
      int i = 0;
      System.out.println("\nChoose a file by number:\n");
      //Goes through the files (as strings) and shows them to the user. User then chooses a file by its associated number.
      for (File stringFile : fileResults){
        System.out.println(i + ": " + stringFile.toString());
        i++;
      }
    } catch (IOException e){
      e.printStackTrace();
    };

    Scanner input = new Scanner(System.in);
    try{
      //User chooses file, which corresponds to the array-index to the file.
      int choice = input.nextInt();
      chosenFile = fileResults.get(choice);
    } catch (Exception e){
      System.out.println("Please input a valid number!");
    };

    return chosenFile;
  };

  public void listAllFiles(String current){
    //Walks through all the files in all the provided directory and prints them to console.
    try (Stream<Path> walk = Files.walk(Paths.get(current))) {
      List<String> result = walk.filter(Files::isRegularFile)
        .map(x -> x.toString()).collect(Collectors.toList());
        result.forEach(System.out::println);
      } catch (IOException e) {
        e.printStackTrace();
      };
  };

  public void filterFilesByExtension(String current, String extension){
    //Creates filter of which to filter all the files in the provided directory with.
    FilenameFilter filter = new FilenameFilter() {
        @Override
        public boolean accept(File f, String name) {
            return name.endsWith(extension);
        }
    };

    //Goes through all the files in the directory and adds only the 'correct' files to the pathlist.
    ArrayList<String> pathStuff = new ArrayList<String>();
    try (Stream<Path> paths = Files.walk(Paths.get(current))) {
      paths.map(path -> path.toString()).filter(f -> f.endsWith(extension))
      .forEach(path -> pathStuff.add(path));
      } catch (IOException e) {
        e.printStackTrace();
      };

      //Checks if there were any files with the provided extension.
      if (pathStuff.size() == 0){
        System.out.println("\nDidn't find any files with that extension!\n");
      } else {
        System.out.println();
        for (String path : pathStuff){
          System.out.println(path);
        }
        System.out.println();
      }
  };
}
