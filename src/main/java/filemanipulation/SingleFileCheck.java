package filemanipulation;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;

public class SingleFileCheck{
  private File chosenFile;

  public SingleFileCheck(File chosenFile){
    this.chosenFile = chosenFile;
  }

  public String getName(){
    return chosenFile.getName();
  }

  public long getSize(){
    //File length = file size in bytes.
    return chosenFile.length();
  }

  public int countStringInFile(String searchingFor){
    //Gets the file extension by making a substring from the final '.'
    String fileName = chosenFile.toString();
    int index = fileName.lastIndexOf('.');
    String extension = "";
    if(index > 0) {
      extension = fileName.substring(index + 1);
    };

    if (extension.equals("txt")){
      try {
        int count = 0;
        FileReader fileIn = new FileReader(chosenFile);
        BufferedReader reader = new BufferedReader(fileIn);
        String line;
        //While the lines are still a-coming, count the lines that include the search.
        //An issue here is that the function counts ALL inclusions, including partial
        //sub-strings. So on a search of 'the' it will up the count when it finds the word
        //'there' etc. Could use equals, but then it would miss things like '"Dracula"' and 'dracula.'.
        while((line = reader.readLine()) != null) {
          if(line.toLowerCase().contains(searchingFor.toLowerCase())) {
            String[] foundLine = line.split(" ");
            for (String word : foundLine){
              if (word.toLowerCase().contains(searchingFor.toLowerCase())){
                count += 1;
              }
            }
          }
        }
        reader.close();
        return count;
      }catch (Exception e){
        System.out.println(e);
      }
    };
    //Returns -1 to signify it's not a .txt-file.
    return - 1;
  };

  public boolean doesStringExist(String searchingFor){
    //Gets extension again.
    String fileName = chosenFile.toString();
    int index = fileName.lastIndexOf('.');
    String extension = "";
    if(index > 0) {
      extension = fileName.substring(index + 1);
    };

    if (extension.equals("txt")){
      try {
        //Goes through entire .txt, line by line and checks if the line contains the string.
        FileReader fileIn = new FileReader(chosenFile);
        BufferedReader reader = new BufferedReader(fileIn);
        String line;
        while((line = reader.readLine()) != null) {
          if((line.toLowerCase().contains(searchingFor.toLowerCase()))) {
            reader.close();
            return true;
          }
        }
        reader.close();
      }catch (Exception e){
        System.out.println(e);
      }
    };
    return false;
  }

  public int getTxtLineCount(){
    //Once again, gets extension.
    String fileName = chosenFile.toString();
    int index = fileName.lastIndexOf('.');
    String extension = "";
    if(index > 0) {
      extension = fileName.substring(index + 1);
    };

    if (extension.equals("txt")){
      int lines = 0;
      try (BufferedReader reader = new BufferedReader(new FileReader(chosenFile))){
        while (reader.readLine() != null){
          lines += 1;
        }
        reader.close();
      } catch (Exception e){
        System.out.println("Something went wrong!");
      }
      return lines;
    } else {
      //Returns -1, so that I can tell the provided file is not a .txt-file.
      return -1;
    }
  }
}
