//Public packages
import java.util.Scanner;
import java.io.File;
import java.nio.file.Files;

//Packages I made myself
import filemanipulation.*;
import logging.*;

public class Main {
  public static void main(String[] args) {
    mainMenu();
  };

  public static void mainMenu(){
    Scanner input = new Scanner(System.in);
    //Longs, to be used to calculate function execution time.
    long startTime;
    long finishTime;
    long functionExecutionTime;
    //Sets up the interaction of the main menu.
    boolean running = true;
    String choice = "";

    System.out.println("Welcome to the main menu!");
    //Gets the current directory, without the 'out'-folder part.
    String current = System.getProperty("user.dir");
    current = current.replace("out", "");

    //Initalizing the objects that read files and folders.
    LogToFile fileLog = new LogToFile(current);
    FileCheck fileCheckProgram = new FileCheck();

    while (running){
      System.out.println("Press 'q' to end the program.\n");
      System.out.println("0 - List all file names in this directory.");
      System.out.println("1 - List all files by extension.");
      System.out.println("2 - Look at file-info.");

      choice = input.nextLine();
      if (choice.equals("q")){
        running = false;
        System.out.println("Program closing.");

      } else if (choice.equals("0")){
        fileCheckProgram.listAllFiles(current);

      } else if (choice.equals("1")){
        System.out.println("What file extension do you want to search for?");
        System.out.println("(Should be in .png/.jpeg/.txt-format!)");
        String extensionInput = input.nextLine();
        fileCheckProgram.filterFilesByExtension(current, extensionInput);

      } else if (choice.equals("2")){
        //Makes the user chose a file from a list of all files in directory.
        File chosenFile = fileCheckProgram.getSingleFile(current);
        SingleFileCheck getFileInfo = new SingleFileCheck(chosenFile);
        boolean runningSubMenu = true;

        while (runningSubMenu){
          //The user can choose to get name and size of all file types
          //But choices 2 to 4 can only be used (succesfully) on a .txt-file.
          System.out.println("Press 'b' to go back to the main menu!\n");
          System.out.println("0 - Get file name.");
          System.out.println("1 - Get file size.");
          System.out.println("2 - Get .txt-line count.");
          System.out.println("3 - Check if a .txt-file includes a phrase.");
          System.out.println("4 - Check how many times a .txt-file includes a phrase.");
          String subChoice = input.nextLine();

          if (subChoice.equals("b")){
            runningSubMenu = false;
            System.out.println("Going back to main menu!");

          } else if (subChoice.equals("0")){
            //Gets system time at start and finish of function.
            //Uses that to calculate the function execution time.
            //I'm using nanoTime instead of milliseconds because it's more accurate.
            startTime = System.nanoTime();
            //Gets the file name.
            String name = getFileInfo.getName();
            finishTime = System.nanoTime();
            functionExecutionTime = (finishTime-startTime)/1000000;
            //Writes the result + execution time to file and console.
            String message = "Filename is " + name + ". Function took " + functionExecutionTime + "ms to execute.";
            System.out.println(message);
            fileLog.writeToFile(message);

          } else if (subChoice.equals("1")){
            startTime = System.nanoTime();
            //Gets the file size.
            long size = getFileInfo.getSize();
            finishTime = System.nanoTime();
            functionExecutionTime = (finishTime-startTime)/1000000;

            String message = "Filesize is " + size + ". Function took " + functionExecutionTime + "ms to execute.";
            System.out.println(message);
            fileLog.writeToFile(message);

          } else if (subChoice.equals("2")){
            startTime = System.nanoTime();
            //Gets the file line count.
            int lines = getFileInfo.getTxtLineCount();
            finishTime = System.nanoTime();
            functionExecutionTime = (finishTime-startTime)/1000000;
            String message;

            //If-else check for if the file is a .txt-file or has no lines.
            if (lines > 0){
              message = "Text is " + lines + " lines long" + ". Function took " + functionExecutionTime + "ms to execute.";
              System.out.println(message);
              fileLog.writeToFile(message);

            } else if (lines == 0) {
              message = "Text doesn't have any lines! Function took " + functionExecutionTime + "ms to execute.";
              System.out.println(message);
              fileLog.writeToFile(message);

            } else {
              message = "That's not a .txt-file!" + "Function took " + functionExecutionTime + "ms to execute.";
              System.out.println(message);
              fileLog.writeToFile(message);
            }

          } else if (subChoice.equals("3")){
            System.out.println("What do you want to search for?");
            String toBeSearchedFor = input.nextLine();

            startTime = System.nanoTime();
            //Checks if the string is in the file. Returns false even if it's a non-.txt-file.
            boolean exists = getFileInfo.doesStringExist(toBeSearchedFor);
            finishTime = System.nanoTime();
            functionExecutionTime = (finishTime-startTime)/1000000;
            String message;

            if (exists){
              message = "Found " + toBeSearchedFor + " in .txt! Function took " + functionExecutionTime + "ms to execute.";
            } else {
              message = "Did not find " + toBeSearchedFor + " in file! Function took " + functionExecutionTime + "ms to execute";
            };

            System.out.println(message);
            fileLog.writeToFile(message);

          } else if (subChoice.equals("4")){
            System.out.println("What do you want to search for?");
            String toBeSearchedFor = input.nextLine();

            startTime = System.nanoTime();
            //Gets the amount of times a string appears in a .txt-file.
            int stringCount = getFileInfo.countStringInFile(toBeSearchedFor);
            finishTime = System.nanoTime();
            functionExecutionTime = (finishTime-startTime)/1000000;
            String message;

            //If-else check to see if file is a .txt-file or if the string isn't in the .txt at all.
            if (stringCount > 0 ){
              message = "Text includes " + toBeSearchedFor + " " + stringCount + " times. Function took " + functionExecutionTime + "ms to execute.";
              System.out.println(message);
              fileLog.writeToFile(message);

            } else if (stringCount == 0){
              message = "Text doesn't include " + toBeSearchedFor + ". Function took " + functionExecutionTime + "ms to execute.";
              System.out.println(message);
              fileLog.writeToFile(message);

            } else {
              message = "That's not a .txt-file!" + "Function took " + functionExecutionTime + "ms to execute.";
              System.out.println(message);
              fileLog.writeToFile(message);
            }

          } else {
            System.out.println("\nNot a valid option!\n");
          };
        };

      } else {
        System.out.println("\nSorry, not a valid option!\n");
      };
    };
  };
};
