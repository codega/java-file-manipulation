# File Manipulator

Assignment 1 for Experis Academy's Java course. A console application to manipulate files. Reads through the directory it's put in. Can filter files by extension, search through .txt-files and read name and file size of all types of files.

## Getting Started

### Prerequesites
Needs no external packages outside of JDK.

### Compile and run
To run the application, simply do this command in the \out\-folder:
> java -jar FileManipulator.java

![How to compile and run the application](https://gitlab.com/codega/java-file-manipulation/-/raw/master/out/projectpictures/Compiling.png "Compile and run")

## Author

* Charlotte Ødegården
